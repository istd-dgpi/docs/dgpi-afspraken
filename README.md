# DgPi Afspraken

> Werkafspraken binnen het DgPi-initiatief. Deze worden in de loop van de tijd aangepast op de situatie.

In de huidige situatie is er een overgang van *vrij experimenteren* naar het *opleveren  van daadwerkelijk bruikbare producten*. Daarmee zal de documentatie over de samenstelling en het gebruik van DgPi-producten gaan toenemen. 

Vanuit die gedachte dat we **DgPi** als een *Open Ecosysteem* willen positioneren starten we met [Semantisch Versioneren 2.0.0](https://semver.org/lang/nl/#semantisch-versioneren-200) als de basis voor het documentbeheer. Om dit optimaal te ondersteunen laten we ons inspireren op de "*Types of changes*" uit de [keep a changelog 1.0.0](https://keepachangelog.com/nl/1.1.0/) als de GitLab Issue-labels.

Ga voor details naar:

- De gedefinieerde [Labels voor Documenten](https://gitlab.com/groups/istd-dgpi/docs/-/labels)
- De [werkafspraken voor het beheren van documenten](documentbeheer)




